import os
import sys


class Restore:
    host = '127.0.0.1'
    port = '27017'
    username = 'NA'
    password = 'NA'
    authenticationDatabase = 'NA'
    databaseToRestore = 'NA'
    input_dir = os.path.dirname(os.path.realpath(__file__))
    args = ()

    def __init__(self, args=args):
        self.args = args
        if len(self.args) > 0:
            for index, output_arg in enumerate(self.args):
                # It works if the argument "" is not empty
                if self.__void_or_incorrect_arg(index, output_arg, '-H'):
                    self.host = self.args[index + 1]
                if self.__void_or_incorrect_arg(index, output_arg, '-P'):
                    self.port = self.args[index + 1]
                if self.__void_or_incorrect_arg(index, output_arg, '-u'):
                    self.username = self.args[index + 1]
                if self.__void_or_incorrect_arg(index, output_arg, '-p'):
                    self.password = self.args[index + 1]
                if self.__void_or_incorrect_arg(index, output_arg, '-authdb'):
                    self.authenticationDatabase = self.args[index + 1]
                if self.__void_or_incorrect_arg(index, output_arg, '-d'):
                    self.databaseToRestore = self.args[index + 1]
                if self.__void_or_incorrect_arg(index, output_arg, '-i'):
                    self.input_dir = self.args[index + 1]

    def __void_or_incorrect_arg(self, index, arg, flag):
        return arg == flag and index + 2 <= len(self.args)

    def __command_restore(self):
        command = "mongorestore"
        if self.host != 'NA':
            command += " --host {}".format(self.host)
        if self.port != 'NA':
            command += " --port {}".format(self.port)
        if self.username != 'NA':
            command += " --username {}".format(self.username)
        if self.password != 'NA':
            command += " --password {}".format(self.password)
        if self.authenticationDatabase != 'NA':
            command += " --authenticationDatabase={}".format(self.authenticationDatabase)
        if self.databaseToRestore != 'NA':
            command += " -d {}".format(self.databaseToRestore)
        if self.input_dir != 'NA':
            command += " {}".format(self.input_dir)
        return command

    def run_restore(self):
        os.system(self.__command_restore())


if __name__ == "__main__":
    Restore()
