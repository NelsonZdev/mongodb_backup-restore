import os
import time
import subprocess

"""
    # https://gist.github.com/mr-exception/9c6acff536bbba897e1b2bba11acd1ea
"""


class Backup:
    host = '127.0.0.1'
    port = '27017'
    username = 'NA'
    password = 'NA'
    database = 'NA'
    output_dir = os.path.dirname(os.path.realpath(__file__))
    args = ()

    def __init__(self, args=args):
        self.args = args
        if len(self.args) > 0:
            for index, output_arg in enumerate(self.args):
                # It works if the argument "" is not empty
                if self.__void_or_incorrect_arg(index, output_arg, '-H'):
                    self.host = self.args[index + 1]
                if self.__void_or_incorrect_arg(index, output_arg, '-P'):
                    self.port = self.args[index + 1]
                if self.__void_or_incorrect_arg(index, output_arg, '-u'):
                    self.username = self.args[index + 1]
                if self.__void_or_incorrect_arg(index, output_arg, '-p'):
                    self.password = self.args[index + 1]
                if self.__void_or_incorrect_arg(index, output_arg, '-db'):
                    self.database = self.args[index + 1]
                if self.__void_or_incorrect_arg(index, output_arg, '-o'):
                    self.output_dir = self.args[index + 1]

    def __void_or_incorrect_arg(self, index, arg, flag):
        if arg == flag and index + 2 <= len(self.args):
            return self.args[index + 1][0] != flag[0]
        else:
            return False

    def __arg_value(self, index, arg, flag):
        # It works if the argument "" is not empty
        if arg == flag and index + 2 <= len(self.args):
            return self.args[index + 1]
        else:
            return 'NA'

    def __output_file(self):
        if len(self.args) > 0:
            for arg in self.args:
                if arg == '--Project':
                    return "{}/{}-{}".format(self.output_dir, "project_backup", time.strftime("%Y-%m-%d_%H-%M-%S"))
        return "{}/{}-{}".format(self.output_dir, "backup", time.strftime("%Y-%m-%d_%H-%M-%S"))

    def __command(self):
        command = "mongodump"
        if self.host != 'NA':
            command += " --host {}".format(self.host)
        if self.port != 'NA':
            command += " --port {}".format(self.port)
        if self.username != 'NA':
            command += " --username {}".format(self.username)
        if self.password != 'NA':
            command += " --password {}".format(self.password)
        if self.database != 'NA':
            command += " --db={}".format(self.database)
        command += " --out {}".format(self.__output_file())
        return command

    def run_backup(self):
        os.system(self.__command())


if __name__ == "__main__":
    Backup()
