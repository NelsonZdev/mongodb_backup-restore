from src.restore import core_restore
from src.backup import core_backup
import sys


ARGS = list(sys.argv)
TRUE_COMMANDS = {
    'Service': '--Service',
    'Restore': '--Restore',
    'Backup': '--Backup',
    'Help': '--Help'
}


def app():
    if ARGS[1] == TRUE_COMMANDS['Backup']:
        backup = core_backup.Backup(args=ARGS[2:])
        backup.run_backup()
    elif ARGS[1] == TRUE_COMMANDS['Restore']:
        restore = core_restore.Restore(args=ARGS[2:])
        restore.run_restore()
    elif ARGS[1] == TRUE_COMMANDS['Help']:
        print(TRUE_COMMANDS)
    else:
        raise ValueError('{} command not found \nUser --Help for get data'.format(ARGS[1]))
    # print(backup.run_backup())


if __name__ == '__main__':
    if len(ARGS) > 1:
        app()
